﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace foor
{
    class Program
    {
        static void Main(string[] args)
        {
            string värv;
            do
            {
                Console.WriteLine("\nMis värvi tuli on valgusfooris: ");
                värv = Console.ReadLine().ToLower();
                if (värv == "punane" | värv == "red")
                {
                    Console.WriteLine("Peatu!");
                }
                else if (värv == "kollane" | värv == "yellow")
                {
                    Console.WriteLine("Aeglusta, oota rohelist");
                }
                else if (värv == "roheline" | värv == "green")
                {
                    Console.WriteLine("Sõida edasi!");
                }
                else
                {
                    Console.WriteLine("Vaata uuesti");
                }
                
            } while (värv != "roheline" || värv != "green");
        }
    }
}