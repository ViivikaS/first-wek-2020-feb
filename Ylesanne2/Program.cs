﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Ylesanne2
{
    class Program
    {
        static void Main(string[] args)
        {
            //string filename = @"..\..\nimekiri.txt";
            //string sisu = File.ReadAllText(filename);
            //Console.WriteLine(sisu);

            //string[] loetudRead = File.ReadAllLines(filename);
            //for (int i = 0; i < loetudRead.Length; i++)
            //{
            //    Console.WriteLine($"rida {i}´- {loetudRead[i]}");
            //}

            // Spordipäev loe failist nimed ja ütle, kes oli kõige kiirem jooksja
            string jooks = @"..\..\spordipäev.txt";
            // Console.WriteLine(File.ReadAllText(jooks); // kontrollin kas loeb faili

            string[] read = File.ReadAllLines(jooks);
            string[] nimed = new string[read.Length - 1];
            double[] kiirused = new double[read.Length - 1];
            
            int kiireim = 0;
            for (int i = 0; i < nimed.Length; i++)
            {
                string[] tykid = read[i + 1].Replace(", ",",").Split(',');
                nimed[i] = tykid[0];
                kiirused[i] = double.Parse(tykid[1]) / double.Parse(tykid[2]);
                Console.WriteLine($"{nimed[i]} kiirus {kiirused[i]:F2}");
                if (kiirused[i] > kiirused[kiireim]) kiireim = i; // see võrdleb kiiruseid ja jätab mällu igakord kiireima kiiruse
            }

            Console.WriteLine($"\nKõige kiirem on {nimed[kiireim]} oma kiirusega {kiirused[kiireim]:F2}");
            
        }
    }
}
