﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace neljas280220
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random(); // juhusliku arvu genereerija andmetüüp kui anda ette number (), siis on tulem igakord sama vt. Random r = new Random(776069228); 

            // NB! kui selle randomi "tegemisel" anda ette mingi number, siis need juhuslikud asjad on igakord juhuslikult samad

            //int i = r.Next();           // randomi käest saab "küsida" juhulikke arve
            //Console.WriteLine(i);

            //i = r.Next(100);            // juhulikke täisarve KUN SAJANI (excl)
            //Console.WriteLine(i);

            //i = r.Next(10, 20);         // juhulikke arve 10-st (incl) 20ni (excl)
            //Console.WriteLine(i);

            //double d = r.NextDouble();  // juhulikke double-id vahemikus 0 kuni 1 (excl)
            //Console.WriteLine(d);


            int[] arvud = new int[100];
            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = r.Next(1000);
            }

            for (int i = 0; i < arvud.Length; i++)
            {
                Console.Write($"{arvud[i]}{(i % 10 == 9 ? "\n" : "\t")}");
            }

            int suurim = arvud[0];
            int väikseim = arvud[0];
            int summa = 0;
            for (int i = 1; i < arvud.Length; i++)
            {
                suurim = arvud[i] > suurim ? arvud[i] : suurim;
                väikseim = arvud[i] < väikseim ? arvud[i] : väikseim;
                summa += arvud[i];
            }

            Console.WriteLine($"suurim on: {suurim}");
            Console.WriteLine("suurim on: " + suurim.ToString());
            Console.WriteLine($"Väikseim on: {väikseim}");
            Console.WriteLine("väikseim on: {0}", väikseim);
            Console.WriteLine($"Keskmine on: {summa / arvud.Length}");


            int variant = r.Next();
            r = new Random(variant);

            int[,] tabel = new int[10, 10];
            for (int i = 0; i < tabel.GetLength(0); i++)
            {
                for (int j = 0; j < tabel.GetLength(0); j++)
                {
                    tabel[i, j] = r.Next(100);
                    Console.Write(tabel[i, j] + "\t");
                }
            }

        }
    }
}
