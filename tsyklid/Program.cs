﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tsyklid
{
    class Program
    {
        static void Main(string[] args)
        {
            //tsüklid - kui midagi on vaja teha mitu korda
            //for (int i = 0; i < 10; i++) // kui i++ on for lauses lobe alates 0-st 10x
            //{
            //    Console.WriteLine($"Teen seda asja {i}-{(i == 1 || i ==2 ? "st": "ndat")} korda");
            //}

            for (int i = 0; i < 10;)
            {
                Console.WriteLine($"Teen seda asja {++i}-{(i == 1 || i == 2 ? "st" : "ndat")} korda");
            }

            int vastus = 0;
            for (
                Console.WriteLine("mis su palk on: ");
                !int.TryParse(Console.ReadLine(), out vastus);
                Console.WriteLine("Kirjuta ilusti\nmis su palk on: ")
                )
            { }
            Console.WriteLine($"Selge su palk on {vastus}");

            // while tsükkel

            while(DateTime.Now.DayOfWeek != DayOfWeek.Thursday) 
            // for (; DateTime.Now.DayOfWeek != DayOfWeek.Friday;)
            {
                Console.WriteLine("ootan reedet");
                System.Threading.Thread.Sleep(1000);
            }

            // do - tsükkel
            do
            {
                Console.WriteLine("ootan reedet");
                System.Threading.Thread.Sleep(1000);
            } while (DateTime.Now.DayOfWeek != DayOfWeek.Thursday);

            //for(bool b = true; b; b = DateTime.Now.DayOfWeek != DayOfWeek.Thursday)
            //{

            //}
        }
    }
}
