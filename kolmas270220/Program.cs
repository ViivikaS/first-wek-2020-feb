﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kolmas270220
{
    class Program
    {
        static void Main(string[] args)
        {
            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday) // tingimuslik lause
            {
                Console.WriteLine("täna lähme sauna");
                int vihtadeArv = 7;
                Console.WriteLine($"Võtame kaasa {vihtadeArv} vihta");
            }
            else
            {
                //need laused siin blokis täidetakse, kui if-lause on false
                Console.WriteLine("Täna ei lähe sauna");
            }

            //// teeme ühe näite-variandi veel

            //if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
            //{
            //    //laupäev lauseid võib olla mitu
            //    if ()
            //    {

            //    }
            //    else
            //}
            //else if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
            //{

            //}
            //else
            //{

            //}


            // muutuja skoop
            string see = "see on protseduuri tasemel muutuja";
            {
                int kohalik = 5;
            }

            {
                string kohalik = "kaheksa";

            }

           // bool kohalik = true; // nii ei saa


        }

    }
}
