﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Massiivid
{
    class Program
    {
        static void Main(string[] args)
        {
            int i1 = 0;
            int i2 = 7;

            int[] arvud; // tegemist on massiiviga - aga tal pole hetkel sisu
            arvud = new int[10]; // nüüd on massiivil sisu  ta koosneb 10-st int-ist [i2] koosneks ta seitsmest

            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] = i * i; // pöördumine indeksi kaudu
            }

            foreach (var x in arvud) Console.WriteLine(x); // javas on in asemel :

            for (var e = arvud.GetEnumerator(); e.MoveNext();)
            {
                var x = e.Current;
                Console.WriteLine(x);
            }

            int[] teisedArvud = new int[10]; // võib kohe ka väärtuse anda
            int[] kolmandadArvud = teisedArvud; // sellisel juhul 1 massiiv
            //int[] kolmandadArvud = (int[])teisedArvud.Clone(); // sellisel juhul 2 massiivi
            kolmandadArvud[3] = 7;
            Console.WriteLine(teisedArvud[3]);

            int[] veelYks = new int[5] { 1, 3, 5, 2, 7 }; // massiiv ja initializer kui new int[] siis ei pea {} sees olema kindel arv 
            veelYks = new int[] { 7, 2, 3, 8, 9, 1, 0 };

            int[] viimane = { 1, 2, 3, 4, 5, 6, 78 };

            // mõned massiivid veel

            string[] kuud =
            {
                "jaanuar",
                "veebruar",
                "märts",
                "jaanuar",
                "veebruar",
                "märts",
                "jaanuar",
                "veebruar",
                "märts",
            };

            int[][] paljuarve; // massiivide masiiv [][]
            paljuarve = new int[3][];
            paljuarve[0] = new int[] { 1, 2, 3, 4 };
            paljuarve[1] = new int[] { 7, 8, 9 };
            paljuarve[2] = new int[] { 5, 6 };
            Console.WriteLine(paljuarve[1][2]); // prindib 9

            int[,] tabel = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 }, { 0, 0, 10 } }; // kahemõõtmeline massiiv [,]
            Console.WriteLine(tabel[2,2]); // prindib 9


        }
    }
}
