﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @switch
{
    class Program
    {
        static void Main(string[] args)
        {
            switch (DateTime.Now.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                    // laused mida täidetakse laupäeval
                    Console.WriteLine("Täna saab sauna");
                    //break;
                    goto case DayOfWeek.Sunday;
                case DayOfWeek.Sunday:
                    Console.WriteLine("joome õlutit");
                    //break;
                    goto default;
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                    // laused, mida täidetakse kolmapäeval
                    Console.WriteLine("lähen sulgpalli");
                    break;
                default:
                    // laused mida täidetakse muudel päevadel
                    Console.WriteLine("tee tööd");
                    break;
            }
        }
    }
}
