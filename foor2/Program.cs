﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace foor2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool foorituli = false;
            do
            {
                Console.WriteLine("\nMis värvi tuli on valgusfooris: ");
                switch (Console.ReadLine().ToLower())
                {
                    case "punane":
                    case "red":
                        Console.WriteLine("Peatu!");
                        
                        break;
                    case "kollane":
                    case "yellow":
                        Console.WriteLine("Aeglusta, oota rohelist!");
                        
                        break;
                    case "roheline":
                    case "green":
                        Console.WriteLine("Sõida edasi!");
                        foorituli = true;
                        break;
                    default:
                        Console.WriteLine("Vaata uuesti!");
                        
                        break;
                }
            } while (foorituli != true);
        }

    }
}
